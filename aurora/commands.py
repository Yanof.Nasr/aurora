import os
import click
from flask.cli import with_appcontext

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')
PROFILER_PATH = os.path.join(PROJECT_ROOT, 'profiler')


@click.command(name='test')
@with_appcontext
def test():
    """Run the tests."""
    import pytest
    print(TEST_PATH)
    rv = pytest.main([TEST_PATH, '--verbose'])
    exit(rv)


@click.command(name='profile')
@with_appcontext
def profile():
    """Profile the code."""
    import cProfile
    import pstats
    import subprocess
    from aurora import aurora

    if not os.path.exists(PROFILER_PATH):
        os.mkdir(PROFILER_PATH)

    prof = cProfile.Profile()

    # "\033[1;32m" Makes the text appear green in the terminal
    print("\033[1;32mProfiling started")

    prof.runctx('aurora.create_app(__name__)', globals(), locals())
    prof.dump_stats(PROFILER_PATH + '/output.prof')

    print("\033[1;32mPreparing output")
    stream = open(PROFILER_PATH + '/output.txt', 'w')
    stats = pstats.Stats(PROFILER_PATH + '/output.prof', stream=stream)
    stats.sort_stats('cumtime')
    stats.print_stats()
    subprocess.run(["py-spy", "record", "-o",
                   "profiler/profile.svg", "--", "python", "app.py"])
    print("\033[1;32mProfiling finished")

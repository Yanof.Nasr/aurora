from aurora.aurora import create_app
import argparse  # Command line parsing library

""" Entry point of the Flask app """
app = create_app(__name__).app

# For development purposes
if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-I", "--ip", type=str, default="localhost",
                    help="ip address of the device")
    ap.add_argument("-P", "--port", type=int, default=5000,
                    help="ephemeral port number of the server (1024 to 65535)")
    ap.add_argument("-D", "--debug", type=bool, default=True,
                    help="run in debug mode")
    args = vars(ap.parse_args())

    app.run(host=args["ip"], port=args["port"], debug=args["debug"])

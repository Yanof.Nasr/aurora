import { makeRequest } from "./utils.js";

export async function initPlaylists(){
    if (!localStorage.getItem("6Songs")){
        const response = JSON.parse(await makeRequest('GET', '/spot'));

        localStorage.setItem("0Songs", JSON.stringify(response[0].happy_songs));
        localStorage.setItem("1Songs", JSON.stringify(response[1].neutral_songs));
        localStorage.setItem("2Songs", JSON.stringify(response[2].sad_songs));
        localStorage.setItem("3Songs", JSON.stringify(response[3].angry_songs));
        localStorage.setItem("4Songs", JSON.stringify(response[4].surprise_songs));
        localStorage.setItem("5Songs", JSON.stringify(response[5].disgust_songs));
        localStorage.setItem("6Songs", JSON.stringify(response[6].fear_songs));
    }
}

export function clearPlaylists(){
    localStorage.clear();
}

export function updatePlaylist(mood, songs) {
    const playlistName = mood + "Songs";
    const playlist = JSON.parse(localStorage.getItem(playlistName));

    for (let song in songs){
        if (songInPlaylist(song, playlist))
            continue;
        playlist.push(song);
    }

    localStorage.setItem(playlistName, JSON.stringify(playlist));
}

export function getPlaylist(mood) {
    const playlistName = mood + "Songs";
    return JSON.parse(localStorage.getItem(playlistName));
}

function songInPlaylist(song, playlist){
    const found = playlist.find(element => element === song);
    return found != null;
}
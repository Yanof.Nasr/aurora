export const Mood = {
    HAPPY: 0,
    NEUTRAL: 1,
    SAD: 2,
    ANGRY: 3,
    SURPRISE: 4,
    DISGUST: 5,
    FEAR: 6
}

export const State = {
    moodQueue: Array(3).fill(Mood.NEUTRAL),  

    consistentMood: Mood.NEUTRAL,
    songIsMatching: false
}

export function setMood(moodStr) {
    const mood = stringToMood(moodStr);

    State.moodQueue.push(mood);
    State.moodQueue.shift();

    if (consistentMood()){
        if (State.consistentMood !== mood) {
            State.songIsMatching = false;
            console.log("Mood changed! You are now " + moodToString(mood)); // not using moodStr because it isn't a normalized value
        }
        State.consistentMood = mood;
    }
}

export function getMood() {
    return State.consistentMood;
}

export function songIsMatching() {
    return State.songIsMatching;
}

export function matchedSong(){
    State.songIsMatching = true;
}

function consistentMood() {
    return new Set(State.moodQueue).size === 1;
}

export function stringToMood(moodStr) {
    let mood = 0;

    switch (moodStr){
        case 'happy':
            mood = Mood.HAPPY;
            break;
        case 'neutral':
            mood = Mood.NEUTRAL;
            break;
        case 'sad':
            mood = Mood.SAD;
            break;
        case 'angry':
            mood = Mood.ANGRY;
            break;
        case 'surprise':
            mood = Mood.SURPRISE;
            break;
        case 'disgust':
            mood = Mood.DISGUST;
            break;
        case 'fear':
            mood = Mood.FEAR;
            break;
        default:
            mood = getMood();
    }

    return mood;
}

export function moodToString(moodNum) {
    let mood = '';

    switch (moodNum){
        case Mood.HAPPY:
            mood = 'happy';
            break;
        case Mood.NEUTRAL:
            mood = 'neutral';
            break;
        case Mood.SAD:
            mood = 'sad';
            break;
        case Mood.ANGRY:
            mood = 'angry';
            break;
        case Mood.SURPRISE:
            mood = 'surprised';
            break;
        case Mood.DISGUST:
            mood = 'disgusted';
            break;
        case Mood.FEAR:
            mood = 'scared';
            break;
        default:
            mood = moodToString(getMood());
    }

    return mood;
}
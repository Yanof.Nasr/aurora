/**
 * Utility function for promisifying XMLHttpRequests
 * @param {String} method 
 * @param {String} url 
 * @param {*} data 
 * @returns Promise of the server resonse
 */
 export async function makeRequest(method, url, data = null) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState != 4) return;

            if (xhr.status < 200 || xhr.status > 299) {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }

            resolve(xhr.responseText);
        };
        xhr.onerror = function () {
            reject({
                status: xhr.status,
                statusText: xhr.statusText
            });
        };
        xhr.send(data);
    });
}

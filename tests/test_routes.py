import pytest
from aurora.aurora import create_app

@pytest.fixture
def client():
    app = create_app("TESTING")
    flask = app.app
    client = flask.test_client()
    return client

def test_route_index(client):
    response = client.get('/')
    assert response.status_code == 200
    assert len(response.history) == 0

def test_route_favicon(client):
    response = client.get('/favicon.ico')
    assert response._status_code == 200
    assert len(response.history) == 0

# TODO: fix the following test cases
# def test_route_proc(client):
#     response = client.post('/proc', data=None)
#     assert response._status_code == 201
#     assert response.content == 'nothing'
#     assert len(response.history) == 0
# def test_route_spot(client):
#     response = client.get('/spot')
#     assert response._status_code == 201
#     assert len(response.content) == 7
#     assert len(response.history) == 0
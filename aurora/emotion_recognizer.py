from fer import FER
import numpy as np
from PIL import Image
import cv2
import io
import base64

class EmotionRecognizer:
    def __init__(self):
        self.emotion_recognizer = FER(min_neighbors=3)
        
    def detect_emotion(self, base64_string):
        """ Classifies the detection of emotion and it registers the output into seven categories
        namely, "surprise", "fear", "neutral", "happy", "sad", "anger", and "disgust". """
        if base64_string is None:
            return "nothing"

        # Process a single frame of video and setting the Region Of Interest (ROI)
        frame = bgr_to_rgb(string_to_image(base64_string))
        emotion, score = self.emotion_recognizer.top_emotion(frame)

        if emotion is None:
            return "nothing"

        return emotion

def string_to_image(base64_string):
    """Take in base64 string and return PIL image"""
    imgdata = base64.b64decode(base64_string)
    return Image.open(io.BytesIO(imgdata))

def bgr_to_rgb(image):
    """Convert BGR encoded image to RGB"""
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)

import { matchedSong, songIsMatching, getMood} from "./state.js";
import { getPlaylist } from "./playlists.js";

export class Music {
    constructor(music) {
        this.music = music;
        this.playlist = null;
        this.timer = 0;
        this.intervalID = null;
    }

    play() {
        this.music.volume = 0.2;
        this.matchPlaylist();
        this.newSong();
        this.matchMood();
        matchedSong();
    }

    pause() {
        this.music.pause();
        clearInterval(this.intervalID);
    }

    setVolume(volume) {
        this.music.volume = volume;
    }

    matchPlaylist() {
        this.playlist = getPlaylist(getMood()); 
    }

    newSong() {
        const rand = Math.floor(Math.random() * this.playlist.length);
        let track = this.playlist[rand];
        console.log('Now playing ' + track['name']);
        this.music.src = track['preview_url'];
        this.timer = Date.now();
        this.music.play();
    }

    matchMood() {
        const self = this;
        self.intervalID = setInterval(()=> {
            if (!songIsMatching()) {
                self.matchPlaylist();
                self.newSong();
                matchedSong();
            } else if (Date.now() - self.timer >= 30000) {
                self.newSong()
            }
        }, 0);
    }
}
import os
import requests
import time

AUTH_URL = 'https://accounts.spotify.com/api/token'
BASE_URL = 'https://api.spotify.com/v1/'

HAPPY_SONGS     = '36FQKI2fwUoJVn8kVtY0Wk'
NEUTRAL_SONGS   = '3ye8xQdWbQ0kgm7EkcbWKb' #artist id
SAD_SONGS       = '38BAFacbwA5kGf7iqcW7Wj'
ANGRY_SONGS     = '1yMlpNGEpIVUIilZlrbdS0'
DISGUST_SONGS   = '5KQt6XboDDI1JcCe4rUnXr'
SURPRISE_SONGS  = '4o5SxWNsTNLOi9ahHeJF8A'
FEAR_SONGS      = '06Gh1LqdIfG1HMHKa44B7k'

class SpotifyAPI:
    def __init__(self):
        self.client_id = os.environ['SPOTIFY_CLIENT_ID']
        self.client_secret = os.environ['SPOTIFY_CLIENT_SECRET']
        self.token = None
        self.token_expires_at = None
        self.headers = None

    def authenticate(self):
        response = requests.post(AUTH_URL, {
            'grant_type': 'client_credentials',
            'client_id': self.client_id,
            'client_secret': self.client_secret
        })

        if response.status_code != 200:
            print("Spotify API token request failed")
            print(response.status_code)
            print(response.json())
            return
        
        data = response.json()

        self.token_expires_at = int(time.time()) + data['expires_in'] 
        self.token = data['access_token']
        self.headers = { 'Authorization': 'Bearer ' + self.token }

    def get_tracks(self):
        self.refresh_token()

        happy    = self.playlist_tracks(HAPPY_SONGS)
        neutral  = self.artist_top_tracks(NEUTRAL_SONGS)
        sad      = self.playlist_tracks(SAD_SONGS)
        angry    = self.playlist_tracks(ANGRY_SONGS)
        surprise = self.playlist_tracks(SURPRISE_SONGS)
        disgust  = self.playlist_tracks(DISGUST_SONGS)
        fear     = self.playlist_tracks(FEAR_SONGS)

        tracks = [
            {"happy_songs":happy}, 
            {"neutral_songs":neutral}, 
            {"sad_songs":sad}, 
            {"angry_songs":angry}, 
            {"surprise_songs":surprise},
            {"disgust_songs":disgust},
            {"fear_songs":fear}
        ]

        return tracks

    def playlist_tracks(self, playlist_id):
        response = requests.get(
            url = BASE_URL + 'playlists/' + playlist_id + '/tracks', 
            headers = self.headers,
            params = {
                'fields': 'items',
                'limit': '50',
                'market': 'US'
            }
        )
        filtered = [obj['track'] for obj in response.json()['items'] if obj['track']['preview_url'] is not None]
        return filtered
    
    def artist_top_tracks(self, artist_id):
        response = requests.get(
            url = BASE_URL + 'artists/' + artist_id + '/top-tracks', 
            headers = self.headers,
            params = {
                'market': 'US'
            }
        )
        filtered = [obj for obj in response.json()['tracks'] if obj['preview_url'] is not None]
        return filtered

    # Checks to see if token is valid and gets a new token if not
    def refresh_token(self):
        # Checking if token has expired, with a 60 second refresh window
        now = int(time.time())
        token_expired = self.token_expires_at - now < 60

        # Refreshing token with 3 tries if it has expired
        if token_expired:
            self.token = None
            tries = 3
            while tries > 0 and self.token is None:
                self.authenticate()

        if self.token is None:
            raise Exception('Could not refresh token')

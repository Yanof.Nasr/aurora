let ctx;
let audioSource;
let myReq;

const audio = document.querySelector('#music');

const upper = document.querySelector(".upper-container");
const lower = document.querySelector(".lower-container");
const wave = document.querySelector(".wave");

function audioVisual() {

    let colors = [];

    let l = Array.from(document.querySelector(".aur_cont").children);
    l.forEach((e) => {
        let box_shadow_str = window.getComputedStyle(e).getPropertyValue('box-shadow');
        colors.push(box_shadow_str);
    })

    // The number of bars that should be displayed
    const NBR_OF_BARS = ((innerWidth/(25+0.2))*0.87).toFixed();

    // Create an audio context
    ctx = ctx || new AudioContext();

    // Create an audio source
    audioSource = audioSource || ctx.createMediaElementSource(audio);

    // Create an audio analyzer
    const analayzer = ctx.createAnalyser();

    // Connect the source, to the analyzer, and then back the the context's destination
    audioSource.connect(analayzer);
    audioSource.connect(ctx.destination);

    // Print the analyze frequencies
    const frequencyData = new Uint8Array(analayzer.frequencyBinCount);
    analayzer.getByteFrequencyData(frequencyData);

    // Create a set of pre-defined bars
    let cs = [];
    let c;
    for( let i = 0; i < NBR_OF_BARS; i++ ) {
        const bar = document.createElement("DIV");
        bar.setAttribute("id", "bar" + i);
        bar.setAttribute("class", "visualizer-container__bar");
        
        c = `${colors[Math.floor(Math.random()*colors.length)]}`;
        cs.push(c);
        bar.style.boxShadow = c;
        upper.appendChild(bar);


        
        const mirrored = document.createElement("DIV");
        mirrored.setAttribute("id", "m_bar" + i);
        mirrored.setAttribute("class", "visualizer-container__m_bar");

        mirrored.style.boxShadow = cs.shift();
        lower.appendChild(mirrored);
    }
    
    // This function has the task to adjust the bar heights according to the frequency data
    function renderFrame() {
        wave.setAttribute("height",innerHeight/10);
        wave.setAttribute("width",innerWidth/10);
        let change = ((innerWidth/(25+0.2))*0.85).toFixed();
        let N = change - upper.children.length;
        if (N < 0) {
            for (let i = N; i < 0; i++) {
                upper.removeChild(upper.lastChild);
                lower.removeChild(lower.lastChild);
            }
        } else if (N > 0) {
            for (let i = 0; i < N; i++) {
                const bar = document.createElement("DIV");
                bar.setAttribute("id", "bar" + upper.children.length);
                bar.setAttribute("class", "visualizer-container__bar");

                c = `${colors[Math.floor(Math.random()*colors.length)]}`;
                cs.push(c);
                bar.style.boxShadow = c;
                upper.appendChild(bar);

                const mirrored = document.createElement("DIV");
                mirrored.setAttribute("id", "m_bar" + lower.children.length);
                mirrored.setAttribute("class", "visualizer-container__m_bar");

                mirrored.style.boxShadow = cs.shift();
                lower.appendChild(mirrored);
            }   
        }

        // Update our frequency data array with the latest frequency data
        analayzer.getByteFrequencyData(frequencyData);

        // The frequency array contains data for 655 frequencies, we don't want to fetch 
        // the first NBR_OF_BARS of values, but try and grab frequencies over the whole spectrum
        const increment = Math.floor(655/(change));

        for( let i = 0; i < change; i++) {
            const index = i * increment;
            // fd is a frequency value between 0 and 255
            const fd = frequencyData[index];

            // Fetch the bar DIV element
            const bar = document.querySelector("#bar" + i);
            const m_bar = document.querySelector("#m_bar" + i);

            // If fd is undefined, default to 0, then make sure fd is at least 4
            // This will make make a quiet frequency at least 4px high for visual effects
            const barHeight = normalize(fd);
            bar.style.height = barHeight + "px";
            m_bar.style.height = barHeight + "px";

        }

        // At the next animation frame, call ourselves
        myReq = window.requestAnimationFrame(renderFrame);

    }

    renderFrame();
}

function normalize(val) {
    const maxVal = 255; // min = 0
    const minOut = 5;   // should scale with the screen pixels of the user
    const maxOut = 200; // should scale with the screen pixels of the user
    return minOut + Math.ceil((val / maxVal)*(maxOut-minOut));
}

function cancelAnimation(){
    cancelAnimationFrame(myReq);
}

export{
    audioVisual,
    cancelAnimation
}

import { makeRequest } from "./utils.js";
import { setMood } from "./state.js";

export class Camera {
    constructor(video) {
        this.video = video;
        this.intervalID = null;

        // The width and height of the captured photo. This corresponds
        // with the width and height of the "inputStream" HTML DOM node.
        this.width = this.video.getAttribute('width');
        this.height = this.video.getAttribute('height');

        // Off-screen canvas object where we will be capturing still
        // images, converting and sending them to the server.
        this.canvas = document.createElement('canvas');
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.context = this.canvas.getContext('2d');
    }

    /** 
     * Starts the video capture of the client camera
     */
    async startCam() {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
            this.video.srcObject = stream;
            this.video.play();
        } catch (err) {
            console.error("Could not open camera: " + err);
            return;
        }

        // Starts a recurring function which will repeatedly send
        // pictures to the server
        this.takePicture();
    }

    /**
     * Stops the video capture of the client camera
     */
    endCam() {
        clearInterval(this.intervalID);
        this.video.srcObject.getTracks().forEach(function(track) {
            track.stop();
        });
        this.video.srcObject = null;
    }

    /**
     * Capture a photo by fetching the current contents of the video
     * and drawing it into a canvas, then converting that to a JPEG
     * format data URL which is then sent to the server for emotion
     * recognition processing. 
     */
    takePicture() {
        this.intervalID = setInterval(async()=>{
            this.context.drawImage(this.video, 0, 0, this.width, this.height);

            // convert image to base64 string
            let data = this.canvas.toDataURL('image/jpeg');
            // remove the unnecessary info from the beginning
            data = data.replace('data:image/jpeg;base64,', '');

            // send to base64 string to the server
            const response = await makeRequest('POST', '/proc', data);
            if (response !== null) {  
                setMood(response);
            }
        }, 1000/3)
    }
}

from aurora.emotion_recognizer import EmotionRecognizer
from aurora.spotify import SpotifyAPI
from aurora import commands

from flask import Flask, Response, render_template, request, send_from_directory, redirect
import os
import random
import sys
import json

class Aurora:
    def __init__(self, name="aurora", *args, **kw):
        """ The flask object implements a WSGI application and acts as the central object.
        It is passed the name of the module or package of the application. 
        Once it is created it will act as a central registry for the view functions,
        the URL rules, template configuration and more. """
        self.app = Flask(name, *args, **kw)
        self.app.cli.add_command(commands.test)
        self.app.cli.add_command(commands.profile)
        self.after_request()
        self.er = EmotionRecognizer()
        self.sp = SpotifyAPI()
        self.route_urls()

    def run(self, host=None, port=None, debug=None):
        self.app.run(host=host, port=port, debug=debug, use_reloader=False)

    def route_urls(self):
        @self.app.route("/")
        def index():
            self.sp.authenticate()
            return render_template("index.html")

        @self.app.route("/proc", methods = ['POST'])
        def save_image():
            base64_string = request.data
            emotion = self.er.detect_emotion(base64_string)
            return Response(emotion, status=201, mimetype='application/json')

        @self.app.route("/spot", methods = ['GET'])
        def get_tracks():
            tracks = self.sp.get_tracks()
            return Response(json.dumps(tracks), status=201, mimetype='application/json')

        @self.app.route("/favicon.ico")
        def favicon():
            return send_from_directory(os.path.join(
                                self.app.root_path, 'aurora', 'static', 'img'),
                                'favicon.ico', mimetype='image/png')
    
    def after_request(self):
        @self.app.after_request
        def add_header(response):
            response.cache_control.max_age = 1
            return response

def create_app(name):
    return Aurora(name, template_folder="aurora/templates", static_folder="aurora/static")

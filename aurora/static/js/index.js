import { Camera } from "./camera.js";
import { Music } from "./music.js";
import { initPlaylists, clearPlaylists  } from "./playlists.js";
import { audioVisual, cancelAnimation } from './visualizer.js'
import { background, start, pause } from './startingPage.js'

const giantAurora = document.querySelector('#giantAurora');
const tinyAurora = document.querySelector('#tinyAurora');
const cameraElement = document.querySelector('#camera');
const musicElement = document.querySelector('#music')
const canvasDiv = document.querySelector('#canvas_holder');
const auroraLights = document.querySelector('#background');
const visualizerHolder = document.querySelector('#content');
const upper = document.querySelector(".upper-container")
const lower = document.querySelector(".lower-container")
const backPic = document.querySelector("#back")

let camera = null;
let music = null;

giantAurora.style.cursor = "pointer";
tinyAurora.style.cursor = "pointer";

async function init() {
    console.log("Initializing Aurora");
    background();
    clearPlaylists();
    await initPlaylists();

    camera = new Camera(cameraElement);
    music = new Music(musicElement);

    addClickEvents();
    console.log("Aurora is ready");
}

function addClickEvents() {
    giantAurora.addEventListener('click', animate);
    giantAurora.addEventListener('click', camera.startCam.bind(camera));
    giantAurora.addEventListener('click', music.play.bind(music));

    tinyAurora.addEventListener('click', animate);
    tinyAurora.addEventListener('click', camera.endCam.bind(camera));
    tinyAurora.addEventListener('click', music.pause.bind(music));
}

window.onload = function() {
    init();
};

musicElement.addEventListener('mouseover', function() {
    musicElement.classList.remove("hide2");
    musicElement.classList.add("show2");
    
});

musicElement.addEventListener('mouseout', function() {
    musicElement.classList.remove("show2");
    musicElement.classList.add("hide2");
});


function animate() {
    canvasDiv.classList.toggle('inactive');
    giantAurora.classList.toggle('inactive');
    tinyAurora.classList.toggle('inactive');
    backPic.classList.toggle('inactive');

    setTimeout(function() {
        cameraElement.hidden = !cameraElement.hidden;
        if (giantAurora.style.display === "none") {
            giantAurora.style.display = "block";
            tinyAurora.style.display = "none";
            start();
            background();
            auroraLights.className = 'hide';
            visualizerHolder.hidden = true;
            cancelAnimation();
            upper.innerHTML = "";
            lower.innerHTML = "";
        } else {
            giantAurora.style.display = "none";
            tinyAurora.style.display = "block";
            pause();
            canvasDiv.firstElementChild.remove();
            auroraLights.className = 'show';
            visualizerHolder.hidden = false;
            audioVisual();
        }
    }, 250);

}
